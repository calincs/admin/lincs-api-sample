import { initClient } from "@ts-rest/core";
import { contracts } from "@lincs.project/lincs-api-contracts";

console.log("\n LINCS-API sample Node application \n");
const LINCS_API_URL = 'https://lincs-api.lincsproject.ca';
const TEST_URI = 'http://viaf.org/viaf/59931033';

// *******************************************
// Example query with the typed ts-rest client

const client = initClient(contracts.api, {
  baseUrl: LINCS_API_URL,
  baseHeaders: {},
});

const response = await client.person.getPerson({
  body: {
    uri: TEST_URI,
    language: "en"
  },
});

if (response.status == 200) {
  // body is a PersonResponse
  const person = response.body;
  if (person) {
    console.log("The URI belongs to " + person.label);
    console.log("who lived from " + person.birthDate + " to " + person.deathDate);
  }
} else {
  console.log("An error occurred. Response body type is unknown. Dumping here...");
  console.log(response.body);
}

console.log("");

// *******************************************
// Example using normal RESTful fetch (no types)

fetch(LINCS_API_URL + "/api/person", {
  headers: {
    "Accept": "application/json",
    "Content-Type": "application/json",
  },
  method: "POST",
  body: JSON.stringify({
    uri: TEST_URI,
    language: "en"
  })
})
  .then((response) => {
    if (response.ok) {
      response.json().then((obj) => {
        console.log("The URI belongs to " + obj.label);
        console.log("who lived from " + obj.birthDate + " to " + obj.deathDate);
      });
    } else {
      response.json().then((obj) => {
        console.log("An error occurred. This is the response:");
        console.log(obj);
      });
    }
  })
