# LINCS API sample

This is a minimal sample TypeScript project to demonstrate the usage of the *lincs-api* client.

The project uses [Tsup](https://tsup.egoist.dev/) to compile the TypeScript. See the dev site for configuration options.

The LINCS-API client is a set of contracts built with [ts-rest](https://ts-rest.com/). Further details beyond this sample on how to use the *ts-rest* client with the lincs-api contract can be found on their website.

## Run the sample

```bash
npm install
npm run dev
```

You should now be able to see the results of some sample queries in the console.
